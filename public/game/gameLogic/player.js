class Player {

    // mPlayerX = window.innerWidth/2
    mPlayerX = 270;
    mPlayerY = window.innerHeight - 150
    mGlobalPositionX = 0;
    mGlobalPositionY = 0;
    mPlayerDY = 0
    mPlayerAY = 0;
    mPlayerImage = null;
    mLeftTurn = false
    mRightTurn = false
    mPlayerOffset = 0;

    mFrozen = false;


    constructor(myUser,userId) {
        this.mMyUser = myUser;
        this.mUserId = userId;

        if(this.mMyUser){
            document.addEventListener("keydown", (event) => {
                if (event.keyCode == 87) { // w
                    this.mPlayerAY = -0.06;
                } else if (event.keyCode == 65) { // a
                    this.mPlayerDX = -5;
                } else if (event.keyCode == 83) { // s
                    this.mPlayerAY = 0.1;
                } else if (event.keyCode == 68) { // d
                    this.mPlayerDX = 5;
                }
            });
            
            document.addEventListener("keyup", () => {
                this.mPlayerDX = 0;
            });
    
            let img = new Image();
            img.src = "../../assets/images/carGameAssests/blueCar.png";
            this.mPlayerImage = img;

        } else {
            let img = new Image();
            img.src = "../../assets/images/carGameAssests/redCar.png";
            this.mPlayerImage = img;

        }
        
    };
    
    Draw(ctx,offSet=0) {

        ctx.drawImage(this.mPlayerImage, this.mPlayerX, this.mPlayerY-offSet, 75, 100);
        
    };

    setPlayerMovement(playerPosition){
        // console.log("translated Pos: ", playerPosition.x, playerPosition.y)
        this.mPlayerX = playerPosition.x;
        this.mPlayerY = playerPosition.y;
        this.mPlayerDX = playerPosition.dx;
        this.mPlayerDY = playerPosition.dy;
        this.mPlayerAY = playerPosition.ay;
    }
    getUserId(){
        return this.mUserId
    }

    Update() {

        
        this.mPlayerDY += this.mPlayerAY;
        let newSpeed = Math.floor((this.mPlayerDY)*-10)
        App.speed = newSpeed<=0?0:newSpeed
        if(App.speed >=70 && (App.herbStatus!="shocked" &&App.herbStatus!="mad")){
            App.herbStatus="happy"
        } else if(App.herbStatus!="shocked" &&App.herbStatus!="mad"){
            App.herbStatus="normal"
        } 

        
        if (this.mPlayerDY > 0) {
            this.mPlayerDY = 0;
        }
        
        if (this.mPlayerDY < -10) {
            this.mPlayerDY = -10;
        }
        
        if (this.mFrozen) {
            this.mPlayerDY = 0;
            this.mPlayerDX = 0;
        }
        // console.log(this.mPlayerDY
        if(this.mPlayerDY>-1){
            this.mPlayerDX = 0
        }

        // this.mPlayerY += this.mPlayerDY;
        this.mPlayerOffset += this.mPlayerDY;
        let halfScreenSize = window.innerWidth/2
        if(this.mPlayerX + this.mPlayerDX >= 0 && this.mPlayerX+ this.mPlayerDX <= halfScreenSize-90) {
            this.mPlayerX += this.mPlayerDX;
            this.mGlobalPositionX = this.mPlayerX / 100;
        }
    };
    
}

class Tile {
    // mTileX = 0;
    // mTileY = 0;
    mTileImage = null;
    mMapY = 0;
    // mAssetName = "";

    constructor(tileX, tileY, assetName) {
        this.mTileX = tileX;
        this.mTileY = tileY;
        this.mAssetName = assetName;


        let img = new Image();
        img.src = `../../assets/images/carGameAssests/${this.mAssetName}.png`;
        this.mTileImage = img;

        
    }

    Draw(ctx, index, seceatSauce, totalRows) {
        let column = index % 6;
        let row = Math.floor(index / 6);
        // console.log(index,column, row);
        // console.log(this.mTileImage)
        ctx.drawImage(this.mTileImage, column*100, row*100 + seceatSauce*100 - totalRows*100 + this.mMapY -110, 100, 100);
    }

    Update(playerDY) {
        
        // this.mPlayerY += this.mPlayerDY;
        // this.mGlobalPositionY = this.mPlayerY;
        // let halfScreenSize = window.innerWidth/2
        // if(this.mPlayerX + this.mPlayerDX >= 0 && this.mPlayerX+ this.mPlayerDX <= halfScreenSize-90) {
        //     this.mPlayerX += this.mPlayerDX;
        //     this.mGlobalPositionX = this.mPlayerX / 100;
        // }
        this.mMapY -= playerDY;
    };

    SetPlayerOn(player) {
        if (this.mAssetName === "cones") {
            player.mPlayerDY = 0;
            this.mAssetName = "solidRoad";
            App.herbStatus="mad"
            let img = new Image();
            img.src = `../../assets/images/carGameAssests/${this.mAssetName}.png`;
            this.mTileImage = img;
        } else if (this.mAssetName === "potHole") {
            player.mFrozen = true;
            App.herbStatus="shocked"
            setTimeout(() => {
                player.mFrozen = false;
                App.herbStatus="normal"
            }, 1000);
            this.mAssetName = "solidRoad";
            let img = new Image();
            img.src = `../../assets/images/carGameAssests/${this.mAssetName}.png`;
            this.mTileImage = img;
        } else if (this.mAssetName === "finishLine") {
            player.mFrozen = true;
            App.timeStop = true;
            if(App.time<90){
                App.herbStatus="laugh"
            } else {
                App.herbStatus="sad"
            }
            
        } else if (this.mAssetName === "grassEdgeRight" || this.mAssetName === "sideWalkleft") {
            if (player.mPlayerDY < -5) {
                player.mPlayerDY = -5;
            }
        }
    }
    

}