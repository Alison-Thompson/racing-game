

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
};

class WebsocketConnectionManager {
    mSocket = null;
    mOnMessageRecieved = (message) => {};


    CreateWebsocketConnection() {
        let promise = new Promise((resolve, reject) => {
            // this.mSocket = new WebSocket("wss://racing-game-code-camp.herokuapp.com");
            this.mSocket = new WebSocket("ws://localhost:3000");
            
            this.mSocket.onopen = () => {
                resolve();
                this.PingLoop();
            };
            this.mSocket.onerror = reject;
            this.mSocket.onmessage = (event) => {
                const messageData = JSON.parse(event.data)
                if (messageData.responseBody !== "Pong") {
                    this.mOnMessageRecieved(messageData);
                } else {
                    console.log("pinged");
                }
            };
        });
        return promise;
    };

    async PingLoop () {
        while(1) {
            await sleep(30000);
            this.SendMessage({
                request: "Ping",
                requestBody: ""
            });
        }
    };

    SendMessage(message) {
        this.mSocket.send(JSON.stringify(message));
    };

    OnMessageRecieved(func) {
        this.mOnMessageRecieved = func;
    };
};

class WebsocketGameManager {
    mWebsocket;
    inLobby;
    usersInLobby;
    gameStarted;
    lobbies;
    playerPositions;
    gameStatus;

    currentAction;
    preformedAction;

    onLobbyStatusChange;
    onUsersInLobbyChange;
    onAvailableLobbiesChange;
    onPlayerPositionsChange;
    onGameStatusChange;
    whenConnectionReady;

    constructor() {
        this.mWebsocket = new WebsocketConnectionManager();
        this.whenConnectionReady = this.mWebsocket.CreateWebsocketConnection();
        this.mWebsocket.OnMessageRecieved((message) => {
            this.HandleIncomingMessage(message);
        });

        this.inLobby = null;
        this.usersInLobby = null;
        this.gameStarted = false;
        this.lobbies = null;
        this.playerPositions = null;
        this.gameStatus = false;

        this.currentAction = null;
        this.preformedAction = (success) => {};
        this.onLobbyStatusChange = (inLobby) => {};
        this.onUsersInLobbyChange = (users) => {};
        this.onAvailableLobbiesChange = (lobbies) => {};
        this.onPlayerPositionsChange = (pos) => {};
        this.onGameStatusChange = (status) => {};

    }

    SetOnLobbyStatusChange(func) {
        this.onLobbyStatusChange = func;
    };

    SetOnUsersInLobbyChange(func) {
        this.onUsersInLobbyChange = func;
    }

    SetOnAvailableLobbiesChange(func) {
        this.onAvailableLobbiesChange = func;
    }

    SetOnPlayerPositionsChange(func) {
        this.onPlayerPositionsChange = func;
    }

    SetOnGameStatusChange(func) {
        this.onGameStatusChange = func;
    }

    HandleIncomingMessage(message) {
        // console.log(message);
        if (message.type === "update") {
            message.events.forEach(event => {
                this.HandleEvent(event.updatedField, event.updateContent);
            });
            return;
        }

        if (this.currentAction == "Joining Lobby") {
            if (message.type === "success") {
                this.currentAction = null;
                this.inLobby = message.responseBody.lobbyId;
                this.usersInLobby = message.responseBody.users;
                this.preformedAction(message.responseBody.users);
                this.preformedAction = null;
            } else {
                this.currentAction = null;
                this.inLobby = null;
                this.preformedAction(false);
                this.preformedAction = null;
            }
        }
        if (this.currentAction === "Leaving Lobby") {
            if (message.type === "success") {
                this.currentAction = null;
                this.inLobby = null;
                this.preformedAction(true);
                this.preformedAction = null;
            } else {
                this.currentAction = null;
                this.preformedAction(false);
                this.preformedAction = null;
            }
        }
        if (this.currentAction === "Creating Lobby") {
            if (message.type === "success") {
                this.currentAction = null;
                this.inLobby = message.responseBody.lobbyId;
                this.usersInLobby = message.responseBody.users;
                this.preformedAction(message.responseBody.users);
                this.preformedAction = null;
            } else {
                this.currentAction = null;
                this.inLobby = null;
                this.preformedAction(false);
                this.preformedAction = null;
            }
        }
        if (this.currentAction === "Getting Lobbies") {
            if (message.type === "success") {
                this.preformedAction(message.responseBody);
                this.preformedAction = null;
            } else {
                this.preformedAction(null);
                this.preformedAction = null;
            }
        }
        if (this.currentAction === "Starting Game") {
            if (message.type === "success") {
                this.gameStarted = true;
                this.preformedAction(message.responseBody);
                this.preformedAction = null;
            } else {
                this.gameStarted = false;
                this.preformedAction(null);
                this.preformedAction = null;
            }
        }

        if (!this.currentAction) {
            if (this.type === "error" && this.responseBody === "Lobby has been disbanded.") {
                this.inLobby = null;
                this.onLobbyStatusChange(null);
            }
        }
    };

    HandleEvent(updatedField, updateContent) {
        if (updatedField === "UsersInLobby") {
            this.usersInLobby = updateContent;
            this.onUsersInLobbyChange(this.usersInLobby);
        }
        if (updatedField === "Lobbies") {
            this.lobbies = updateContent;
            this.onAvailableLobbiesChange(this.lobbies);
        }
        if (updatedField === "PlayerPosition") {
            this.playerPositions = updateContent;
            this.onPlayerPositionsChange(this.playerPositions);
        }
        if (updatedField === "GameStatus") {
            this.gameStatus = updateContent;
            this.onGameStatusChange(this.gameStatus);
        }
    };

    JoinLobby(lobbyId, password, userName) {
        const promise = new Promise(resolve => {
            this.mWebsocket.SendMessage({
                request: "Join Lobby",
                requestBody: {
                    lobbyId: lobbyId,
                    lobbyPassword: password,
                    userName: userName
                }
            });
            this.currentAction = "Joining Lobby";
            this.preformedAction = resolve;
        });
        return promise;
    };

    LeaveLobby(lobbyId) {
        const promise = new Promise(resolve => {
            this.mWebsocket.SendMessage({
                request: "Leave Lobby",
                requestBody: {
                    lobbyId: lobbyId
                }
            });
            this.currentAction = "Leaving Lobby";
            this.preformedAction = resolve;
        });
        return promise;
    };

    CreateLobby(lobbyName, lobbyPassword, userName) {
        const promise = new Promise(resolve => {
            this.mWebsocket.SendMessage({
                request: "Create Lobby",
                requestBody: {
                    lobbyName: lobbyName,
                    lobbyPassword: lobbyPassword,
                    userName: userName
                }
            });
            this.currentAction = "Creating Lobby";
            this.preformedAction = resolve;
        });
        return promise;
    };

    StartGame(lobbyId) {
        const promise = new Promise(resolve => {
            this.mWebsocket.SendMessage({
                request: "Start Game",
                requestBody: {
                    lobbyId
                }
            });
            this.currentAction = "Starting Game";
            this.preformedAction = resolve;
        });
        return promise;
    };

    UpdatePlayerPosition(x, y, dx, dy, ax, ay) {
        this.mWebsocket.SendMessage({
            request: "Update Position",
            requestBody: {
                x: x,
                y: y,
                dx: dx,
                dy: dy,
                ax: ax,
                ay: ay
            }
        });
    };

    GetLobbies() {
        const promise = new Promise(resolve => {
            this.mWebsocket.SendMessage({
                request: "Get Lobbies"
            });
            this.currentAction = "Getting Lobbies";
            this.preformedAction = resolve;
        });
        return promise;
    };
};

const WebsocketManager = new WebsocketGameManager();

// Before this will work you need to be in a lobby and the game needs to be started in that lobby.
// To do this use Diego's front end system to integrate. or to be lazy do it like this.

// In the hosts tab.
// let usersInLobby = await WebsocketManager.CreateLobby("My Lobby", "test", "Taft");
// const lobbies = await WebsocketManager.GetLobbies();
// WebsocketManager.SetOnUsersInLobbyChange((users) => {
//     usersInLobby = users
// })
// WebsocketManager.SetOnGameStatusChange((status) => {
//     JuegoCanvas.loadPlayers(usersInLobby)
//     if (status) {
//         // start the game
//         JuegoCanvas.Tick();
//     }
// })
// await WebsocketManager.StartGame(lobbies[0].lobbyId);

// // // In the Joiner tab.
// const lobbies = await WebsocketManager.GetLobbies();
// let usersInLobby = await WebsocketManager.JoinLobby(lobbies[0].lobbyId , "test", "Taft2");
// WebsocketManager.SetOnUsersInLobbyChange((users) => {
//     usersInLobby = users
// })
// WebsocketManager.SetOnGameStatusChange((status) => {
//     JuegoCanvas.loadPlayers(usersInLobby)
//     if (status) {
//         // start the game
//         JuegoCanvas.Tick();
//     }
// })




// WebsocketManager.SetOnPlayerPositionsChange((arrayOfPlayerPositions) => {
//     // Update Player Positions
// })


// // DO this like every 10-20 or more so ticks. NOT EVERY TICK
// WebsocketManager.UpdatePlayerPosition(x, y, dx, dy, ax, ay); 