function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
};


var App = new Vue({
    el: '#VueApp',
    vuetify: new Vuetify(),
    data: {
        name: '',
        email: '',
        userName: "",
        password: "",
        select: null,
        colors: [
            'indigo',
            'warning',
            'pink darken-2',
            'red lighten-1',
            'deep-purple accent-4',
        ],
        slides: [
            {
                src: '../assets/images/carGameAssests/blueCar.png',
            },
            {
                src: '../assets/images/carGameAssests/truckBlue.png',
            },
            {
                src: '../assets/images/carGameAssests/redCar.png',
            },
            {
                src: '../assets/images/carGameAssests/greenSUV.png',
            }
        ],
        items: [
        ],
        selectedLobby: null,
        chosenLobby: null,
        host: false,
        gameSettings: true,
        lobby: false,
        game: false,
        lobbyPass: null,
        lobbyName: null,
        currentIndex: 0,
        lobbyUsers: [],
        lobbyUserNames: [],

        hostLobbyName: "",
        hostLobbyPassword: "",
        hostUserName: "",
        player: null,
        username:"Taft",
        otherPlayers:[],
        tilesToRender: null,
        playerWidth: 0,
        playerHeight: 0,
        tileSize: 0,
        lastTick: Date.now(),
        currentTick: null,
        fps: 120,
        canvas: null,
        ctx: null,
        screenWidth: null,
        screenHeight: null,
        viewPortMaxHight: 0,
        playersCurrentIndex: 0,
        mapObjects: [],
        tickCount:0,
        mapList: [
            "FFFFFF",
            "[:|:S>",
            "#:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "*:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[*|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[#|:S>",
            "[:|*S>",
            "[:|:S>",
            "[**:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:*:S>",
            "[:|:S>",
            "[:|*S>",
            "[:|#S>",
            "[:|:S>",
            "[#|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|*S>",
            "[*|:S>",
            "[:|#S>",
            "[:|:S>",
            "[:|:S>",
            "[#|:S>",
            "[:|*S>",
            "[:|:S>",
            "[**:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "#:*:S>",
            "[:|:S>",
            "[:|*S>",
            "[:|#S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[*|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "*:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "#:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[#|:S>",
            "[:|:S>",
            "[:|:S>",
            "[**:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:*:S>",
            "[:|:S>",
            "[:|*S>",
            "[:|#S>",
            "*:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:*:S>",
            "[:|:S>",
            "[:|*S>",
            "[:|#S>",
            "[:|:S>",
            "[:|:S>",
            "[#|:S>",
            "[:|:S>",
            "[:|:S>",
            "[**:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:*:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|#S>",
            "[:|:S>",
            "[:|:S>",
            "[#|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|#S>",
            "[:|:S>",
            "[:|:S>",
            "[#|:S>",
            "[:|:S>",
            "[:|:S>",
            "#:|:S>",
            "[:|#S>",
            "[:|:S>",
            "[:|:S>",
            "[#|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|#S>",
            "[:|:S>",
            "[:|:S>",
            "[#|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:#:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "[:|:S>",
            "BBBBBB",
        ],

        speed: 0,
        time: 0.0,
        start:null,
        timeStop:false,
        herbStatus:"normal",

    },
    methods: {
        pauseSelect: function() {
            var x = document.getElementById("selectAudio"); 
            x.pause();
        },
        playRace: function() {
            var x = document.getElementById("raceAudio"); 
            x.play();
        },
        selectLobby: function (item) {
            this.chosenLobby = item;
        },

        joinLobby: async function () {
            if (this.userName && this.selectLobby) {
                this.lobbyUsers = await WebsocketManager.JoinLobby(this.chosenLobby.lobbyId, this.password, this.userName);

                if (!this.lobbyUsers) {
                    alert("Incorrect Lobby Password. Please fail again :)")
                }

                this.lobbyUserNames = WebsocketManager.usersInLobby.map(user => user.userName);

                WebsocketManager.SetOnUsersInLobbyChange((newUsers) => {
                    this.lobbyUsers = newUsers;
                    this.lobbyUserNames = newUsers.map(user => user.userName);
                });

                this.gameSettings = false;
                this.lobby = true;
            } else {
                alert("Please fill in the inputs.") 
            }
        },

        createLobby: async function () {
            if (this.hostLobbyName && this.hostLobbyPassword && this.hostUserName) {
                this.lobbyUsers = await WebsocketManager.CreateLobby(this.hostLobbyName, this.hostLobbyPassword, this.hostUserName);

                this.lobbyUserNames = WebsocketManager.usersInLobby.map(user => user.userName);

                WebsocketManager.SetOnUsersInLobbyChange((newUsers) => {
                    this.lobbyUsers = newUsers;
                    this.lobbyUserNames = newUsers.map(user => user.userName);
                });

                this.gameSettings = false;
                this.lobby = true;                            
            } else {
                alert("Please fill in the inputs.")
            }
        },

        startGame: async function () {
            this.lobby = false;
            this.gameSettings = false;
            this.game = true;

            
            Vue.nextTick(async () => {
                WebsocketManager.StartGame(WebsocketManager.inLobby);
            });
        },

        Tick: function() {
            //tick code
            window.requestAnimationFrame(this.Tick);
            this.timerRun()
            this.currentTick = Date.now();
            let delta = this.currentTick - this.lastTick;
            let interval = 1000 / this.fps;
        
            if (delta > interval) {
                this.lastTick = this.currentTick - (delta%interval)
                // Draw the frame
                this.Draw();
                this.Update();
            }
            
            this.tickCount += 1
            if (this.tickCount>=30) {
                this.tickCount = 0
                // console.log("sending: ", this.player.mGlobalPositionX, this.player.mGlobalPositionX)
                WebsocketManager.UpdatePlayerPosition(this.player.mGlobalPositionX, this.player.mGlobalPositionY, this.player.mPlayerDX, this.player.mPlayerDY, 0,this.player.mPlayerAY); 
            }
        },

        Draw: function() {
            //draw code
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.viewPortMaxHight = window.innerHeight;
            const numberVisibleRows = Math.ceil(this.viewPortMaxHight/100);
            let startIndex = 0;
            let endIndex = this.mapObjects.length;

            this.tilesToRender = this.mapObjects.slice( startIndex, endIndex)
            for (let index = 0; index < this.tilesToRender.length; index++) {
                const element = this.tilesToRender[index];
                // console.log(tilesToRender.length)
                element.Draw(this.ctx, index, Math.ceil(this.viewPortMaxHight/100), Math.floor(this.mapObjects.length/6));
                
            }
            
            for (let j = 0; j < this.otherPlayers.length; j++) {
                const otherPlayer = this.otherPlayers[j];
                otherPlayer.Draw(this.ctx,this.player.mPlayerOffset);
                
            }
            this.player.Draw(this.ctx);
        },
        Update: function() {
            //update code
            this.player.Update();
            for (let index = 0; index < this.tilesToRender.length; index++) {
                const tile = this.tilesToRender[index];
                tile.Update(this.player.mPlayerDY);
            }

            let yPos = Math.floor(this.mapObjects.length/6) + Math.floor(this.player.mPlayerOffset / 100);
            this.player.mGlobalPositionY = Math.floor(this.mapObjects.length/6) + this.player.mPlayerOffset / 100;
            let xPos = Math.round(this.player.mGlobalPositionX - 0.375);
            if (xPos === 6) {
                xPos = 5;
            }
            const tile = this.mapObjects[(yPos-1)*6 + xPos];
            tile.SetPlayerOn(this.player);
            
        },
        loadPlayers: function(newotherPlayers) {
            
            for (let k = 0; k < newotherPlayers.length; k++) {
                if(newotherPlayers[k].userName != this.userName && newotherPlayers[k].userName != this.hostUserName){
                    newotherPlayers[k].x = newotherPlayers[k].x*100;
                    newotherPlayers[k].y = (newotherPlayers[k].y-Math.floor(this.mapObjects.length/6))*100;
                    this.otherPlayers.push(new Player(false,newotherPlayers[k].userId));
                }
                
            }
        },
        Init: function() {
            //Init code
            this.canvas = document.getElementById("juegoCanvas");
            
            this.screenWidth = window.innerWidth;
            this.screenHeight = window.innerHeight;
            
            this.canvas.width = this.screenWidth/2;
            this.canvas.height = this.screenHeight;
            
            this.ctx = this.canvas.getContext("2d");


            for (let y = 0; y < this.mapList.length; y++) {
                const element = this.mapList[y];
                for (let x = 0; x < element.length; x++) {
                    // const element = array[x];
                    let assetName = "";

                    if (this.mapList[y][x] === ":") {
                        assetName = "dashRoad";
                    } else if (this.mapList[y][x] === "|") {
                        assetName = "solidRoad";
                    } else if (this.mapList[y][x] === "S") {
                        assetName = "sideWalkleft";
                    } else if (this.mapList[y][x] === "<") {
                        assetName = "";
                    } else if (this.mapList[y][x] === "[") {
                        assetName = "roadEdgeLeft";
                    } else if (this.mapList[y][x] === "*") {
                        assetName = "cones";
                    } else if (this.mapList[y][x] === "#") {
                        assetName = "potHole";
                    } else if (this.mapList[y][x] === "Z") {
                        assetName = "";
                    } else if (this.mapList[y][x] === "]") {
                        assetName = "";
                    } else if (this.mapList[y][x] === ">") {
                        assetName = "grassEdgeRight";
                    } else if (this.mapList[y][x] === "F") {
                        assetName = "finishLine";
                    } else if (this.mapList[y][x] === "B") {
                        assetName = "startLine";
                    }
                    this.mapObjects.push(new Tile(x, y, assetName))
                    
                }
            }

            WebsocketManager.SetOnPlayerPositionsChange((NewPlayerPositions) => {
                // console.log("SetOnPlayerPositionsChange",NewPlayerPositions)
                for (let index = 0; index < this.otherPlayers.length; index++) {
                    const otherPlayer = this.otherPlayers[index];
                    // console.log(otherPlayer.getUserId(),NewPlayerPositions.userId)
                    if (otherPlayer.getUserId()==NewPlayerPositions.userId) {
                        // console.log("Recieved: ", NewPlayerPositions.x, NewPlayerPositions.y);
                        NewPlayerPositions.x = NewPlayerPositions.x*100;
                        NewPlayerPositions.y = (NewPlayerPositions.y-Math.floor(this.mapObjects.length/6))*100 + window.innerHeight - 150;
                        otherPlayer.setPlayerMovement(NewPlayerPositions)
                    }
                    
                }
            });
            
            this.player = new Player(true,"");

            // this.Tick();
        },

        timerRun: function() {
            if(this.start==null){
                this.start = Date.now()
            } else if(!this.timeStop) {
                this.time = Math.floor((Date.now() - this.start)*0.001)
            }
            
        },
    },
    created: async function () {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        if (urlParams.has('host')) {
            this.host = true;
        }
        // Called when the Vue app is loaded and ready.
        await WebsocketManager.whenConnectionReady;
        const listOfLobbies = await WebsocketManager.GetLobbies();
        if (listOfLobbies.length === 0) {
            listOfLobbies.push({ lobbyName: "No Lobbies Found" })
        }
        this.items = listOfLobbies;

        WebsocketManager.SetOnAvailableLobbiesChange((lobbies) => {
            this.items = lobbies;
        });

        WebsocketManager.SetOnGameStatusChange(() => {
            this.lobby = false;
            this.gameSettings = false;
            this.game = true;

            
            Vue.nextTick(async () => {
                this.Init();
                let usersInLobby = WebsocketManager.usersInLobby;
                WebsocketManager.SetOnUsersInLobbyChange((users) => {
                    usersInLobby = users;
                })
                this.loadPlayers(usersInLobby);
                this.Tick();
                this.pauseSelect();
                this.playRace();
            });
        });

    }
});