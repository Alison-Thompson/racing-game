// Import Necessary Dependencies.
import express, { Express } from 'express';

import Server from './server_core/server';

// Set up the Next JS Server.
const port: number = parseInt(process.env.PORT, 10) || 3000;


// Set Up Express JS Server.
const expressApp: Express = express();
new Server(expressApp, port);