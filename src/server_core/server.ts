import cors from 'cors';
import express, { Express } from 'express';
import { WebSocketServer } from "ws";
import auxiliaryRoutes from '../routers/auxiliaryRoutes';
import { v4 as uuidv4 } from "uuid";
import dayjs from "dayjs";

type User = {
    connection: any,
    userId: string,
    userName: string,
    lobbyId: string,
    lastMessage: dayjs.Dayjs,
    x: number,
    y: number,
    dx: number,
    dy: number,
    ax: number,
    ay: number
};

type Lobby = {
    lobbyId: string,
    lobbyName: string,
    users: { [userId: string]: User },
    lobbyPassword: string,
    useLobbyPassword: boolean,
    lobbyHost: User,
    gameStarted: boolean
};

type Event = {
    updatedField: string,
    updateContent: any
};

type ServerResponse = {
    type: "success" | "update" | "error";
    responseBody?: any,
    events?: Event[]
};

type ServerRequest = {
    request: string,
    requestBody: any
};

function sleep(ms: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
};


class Server {
    // All server configurations.
    mServerConfig: any;
    mApp: Express;

    mServerName: string;
    mPort: number;

    // CORS configurations.
    mWhitelist: string[];
    mCorsConfig: any;

    // All Websocket Configurations.
    mUsersWithNoLobby: { [userId: string]: User } = {};
    mLobbies: { [lobbyId: string]: Lobby } = {};

    constructor(app: Express, port: number) {

        this.mApp = app;
        this.mPort = port;
        this.mServerName = "Racing Game"
        this.mWhitelist = ["localhost:3000", "https://game.wadeinsights.com"];

        this.mCorsConfig = {
            credentials: true,
            origin: (origin: string, callback: (error: Error, success: boolean) => void) => {
                // REMOVE origin === undefined WHEN DONE TESTING.
                if (this.mWhitelist.indexOf(origin) !== -1 || origin === undefined) {
                    callback(null, true);
                } else {
                    console.log(origin);
                    callback(new Error("Not allowed by CORS."), false);
                }
            }
        };

        this.mApp.use(express.json());

        this.InitalizeRouters();
        this.Listen();
        this.CleanUpOldUsers();

    };

    InitalizeRouters(): void {

        // Set Up All Routers.

        this.mApp.use('/api/auxiliary/', cors(this.mCorsConfig), auxiliaryRoutes);

        const publicDir = express.Router();
        publicDir.use("/", express.static(`${__dirname}/../../public`));
        this.mApp.use("/", cors(this.mCorsConfig), publicDir);

    };

    ConnectNewUser(newUser): void {
        const userId = uuidv4();

        const user: User = {
            connection: newUser,
            userId: userId,
            userName: null,
            lobbyId: null,
            lastMessage: dayjs(),
            x: 0,
            y: 0,
            dx: 0,
            dy: 0,
            ax: 0,
            ay: 0
        };

        this.mUsersWithNoLobby[userId] = user;

        newUser.on("message", (data) => {
            const message = JSON.parse(data);
            this.ReceiveMessageFromUser(user, message);
        });
    };

    ReceiveMessageFromUser(user: User, message: ServerRequest) {
        user.lastMessage = dayjs();

        if (message.request === "Ping") {
            this.MessageUser(user, {
                type: "success",
                responseBody: "Pong"
            });
            return;
        }

        if (message.request === "Join Lobby") { // lobbyId, ?lobbyPassword, userName
            const lobbyId = message.requestBody["lobbyId"];
            const lobbyPassword = message.requestBody["lobbyPassword"];

            if (lobbyId in this.mLobbies) {
                if (this.mLobbies[lobbyId].lobbyPassword && lobbyPassword === this.mLobbies[lobbyId].lobbyPassword) {
                    user.userName = message.requestBody["userName"];
                    this.JoinLobby(user, lobbyId);
                    this.MessageUser(user, {
                        type: "success",
                        responseBody: {
                            lobbyId: lobbyId,
                            users: this.GetUsersInLobby(this.mLobbies[lobbyId])
                        }
                    });
                } else if (!this.mLobbies[lobbyId].lobbyPassword) {
                    user.userName = message.requestBody["userName"];
                    this.JoinLobby(user, lobbyId);
                    this.MessageUser(user, {
                        type: "success",
                        responseBody: {
                            lobbyId: lobbyId,
                            users: this.GetUsersInLobby(this.mLobbies[lobbyId])
                        }
                    });
                } else {
                    this.MessageUser(user, {
                        type: "error",
                        responseBody: "Failed to join lobby. Invalid Lobby ID or Lobby Password."
                    });
                }
            }
        } else if (message.request === "Leave Lobby") { // lobbyId
            this.LeaveLobby(user, message.requestBody["lobbyId"]);
            this.MessageUser(user, {
                type: "success",
                responseBody: null
            });
        } else if (message.request === "Create Lobby") { // lobbyName, ?lobbyPassword
            const lobbyId = this.CreateLobby(user, message.requestBody["lobbyName"], message.requestBody["lobbyPassword"]);
            user.userName = message.requestBody["userName"];
            this.JoinLobby(user, lobbyId);
            this.MessageUser(user, {
                type: "success",
                responseBody: {
                    lobbyId: lobbyId,
                    users: this.GetUsersInLobby(this.mLobbies[lobbyId])
                }
            });
        } else if (message.request === "Get Lobbies") { // Nothing
            const lobbies = Object.entries(this.mLobbies).map(([lobbyId, lobby]) => {
                return {
                    lobbyId: lobbyId,
                    lobbyName: lobby.lobbyName,
                    hasPassword: lobby.useLobbyPassword
                }
            });
            this.MessageUser(user, {
                type: "success",
                responseBody: lobbies
            });
        } else if (message.request === "Start Game") { // lobbyId
            const lobbyId = message.requestBody["lobbyId"];
            this.StartGame(user, this.mLobbies[lobbyId]);
            this.MessageUser(user, {
                type: "success",
                responseBody: "Let the games begin!"
            });
        } else if (message.request === "Update Position") { // x, y, dx, dy, ax, ay
            // console.log("Update Position")
            user.x  = message.requestBody.x;
            user.y  = message.requestBody.y;
            user.dx = message.requestBody.dx;
            user.dy = message.requestBody.dy;
            user.ax = message.requestBody.ax;
            user.ay = message.requestBody.ay;
        } else {
            this.MessageUser(user, {
                type: "error",
                responseBody: "Unknown Request."
            });
        }
    };

    CreateLobby(user: User, lobbyName: string, lobbyPassword?: string) {
        const lobbyId = uuidv4();
        this.mLobbies[lobbyId] = {
            lobbyId: lobbyId,
            lobbyHost: user,
            lobbyName: lobbyName,
            lobbyPassword: lobbyPassword,
            useLobbyPassword: !!lobbyPassword,
            users: {},
            gameStarted: false
        };
        this.BroadcastToUsersWithNoLobby({
            type: "update",
            events: [
                {
                    updatedField: "Lobbies",
                    updateContent: Object.entries(this.mLobbies).map(([lobbyId, lobby]) => {
                        return {
                            lobbyId: lobbyId,
                            lobbyName: lobby.lobbyName,
                            hasPassword: lobby.useLobbyPassword
                        }
                    })
                }
            ]
        });
        return lobbyId;
    };

    StartGame(user: User, lobby: Lobby) {
        if (lobby.lobbyHost.userId === user.userId) {
            lobby.gameStarted = true;
            this.serverTick(lobby);
            this.BroadcastToLobby(lobby, {
                type: "update",
                events: [
                    {
                        updatedField: "GameStatus",
                        updateContent: true
                    }
                ]
            });
        }
    };

    JoinLobby(user: User, lobbyId: string) {
        delete this.mUsersWithNoLobby[user.userId]
        user.lobbyId = lobbyId;
        this.mLobbies[lobbyId].users[user.userId] = user;
        this.BroadcastToLobby(this.mLobbies[lobbyId], {
            type: "update",
            events: [
                {
                    updatedField: "UsersInLobby",
                    updateContent: this.GetUsersInLobby(this.mLobbies[lobbyId])
                }
            ]
        });
    };

    LeaveLobby(user: User, lobbyId: string) {
        if (!(lobbyId in this.mLobbies)) {
            return;
        }
        delete this.mLobbies[lobbyId].users[user.userId];
        user.lobbyId = null;
        this.mUsersWithNoLobby[user.userId] = user;

        this.BroadcastToLobby(this.mLobbies[lobbyId], {
            type: "update",
            events: [
                {
                    updatedField: "UsersInLobby",
                    updateContent: this.GetUsersInLobby(this.mLobbies[lobbyId])
                }
            ]
        });

        if (user.userId === this.mLobbies[lobbyId].lobbyHost.userId) {
            for (const [_, lobbyUser] of Object.entries(this.mLobbies[lobbyId].users)) {
                this.LeaveLobby(lobbyUser, lobbyId);
                this.MessageUser(lobbyUser, {
                    type: "error",
                    responseBody: "Lobby has been disbanded."
                });
            }
        }
        console.log(lobbyId,this.mLobbies,this.mLobbies[lobbyId].users)

        if (Object.entries(this.mLobbies[lobbyId].users).length === 0) {
            delete this.mLobbies[lobbyId];
            // Lobby Disbanded.
            this.BroadcastToUsersWithNoLobby({
                type: "update",
                events: [
                    {
                        updatedField: "Lobbies",
                        updateContent: Object.entries(this.mLobbies).map(([lobbyId, lobby]) => {
                            return {
                                lobbyId: lobbyId,
                                lobbyName: lobby.lobbyName,
                                hasPassword: lobby.useLobbyPassword
                            }
                        })
                    }
                ]
            });
        }
    };

    GetUsersInLobby(lobby: Lobby) {
        return Object.entries(lobby.users).map(([userId, user]) => {
            return {
                userId: userId,
                userName: user.userName
            }
        });
    };

    MessageUser(user: User, message: ServerResponse) {
        user.connection.send(JSON.stringify(message));
    };

    BroadcastToLobby(lobby: Lobby, message: ServerResponse) {
        for (const [_, user] of Object.entries(lobby.users)) {
            this.MessageUser(user, message);
        }
    };

    BroadcastToUsersWithNoLobby(message: ServerResponse) {
        for (const [_, user] of Object.entries(this.mUsersWithNoLobby)) {
            this.MessageUser(user, message);
        }
    }

    async CleanUpOldUsers() {
        while (1) {
            await sleep(90000);
            for (const [userId, user] of Object.entries(this.mUsersWithNoLobby)) {
                if (user.lastMessage < dayjs().subtract(60, "seconds")) {
                    delete this.mUsersWithNoLobby[userId];
                }
            }
            for (const [lobbyId, _] of Object.entries(this.mLobbies)) {
                for (const [userId, user] of Object.entries(this.mLobbies[lobbyId].users)) {
                    if (user.lastMessage < dayjs().subtract(90, "seconds")) {
                        this.LeaveLobby(user, lobbyId);
                        delete this.mUsersWithNoLobby[userId];
                    }
                }
            }
        }
    };
    
    async serverTick(lobby: Lobby) {
        while (lobby.gameStarted) {
            await sleep(100);
            this.BroadcastToLobby(lobby, {
                type: "update",
                events: Object.entries(lobby.users).map(([_, user]) => {
                    return {
                        updatedField: "PlayerPosition",
                        updateContent: {
                            userId: user.userId,
                            userName: user.userName,
                            x: user.x,
                            y: user.y,
                            dx: user.dx,
                            dy: user.dy,
                            ay: user.ay,
                            ax: user.ax
                        }
                    }
                })
            });
        }
    }

    Listen(): void {

        const httpServer = this.mApp.listen(this.mPort, () => {
            console.log(`${this.mServerName} is active.`);
            console.log(`${this.mServerName} is listening on port ${this.mPort}.`);
        });

        const websocketServer = new WebSocketServer({ server: httpServer });

        websocketServer.on("connection", (newUser) => {
            this.ConnectNewUser(newUser);
        });
    };

};

export default Server;