import express, { Request, Response } from 'express';
const router = express.Router();

router.use(express.static(__dirname + '/public/gameLogic'));

router.get("/ping", (_req: Request, res: Response) => {
    res.json("pong").status(200);
});

export default router;