FROM node:16-alpine
COPY src/ /app/src/
COPY package-lock.json /app
COPY package.json /app
COPY tsconfig.json /app
COPY public/ /app/public/
WORKDIR /app
RUN npm install
RUN npm run build
CMD npm start